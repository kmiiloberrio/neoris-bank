FROM openjdk:17.0.1-jdk-slim
LABEL authors="kmiiloberrio"
EXPOSE 8080
RUN mkdir -p /app/
ADD build/libs/neoris-bank-0.0.1-SNAPSHOT.jar /app/neoris-bank-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "/app/neoris-bank-0.0.1-SNAPSHOT.jar"]