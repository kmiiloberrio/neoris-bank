package local.technicaltest.neorisbank.movement.application.account_status;

import jakarta.persistence.criteria.Predicate;
import local.technicaltest.neorisbank.movement.domain.Movement;
import local.technicaltest.neorisbank.movement.infrastructure.persistence.MovementRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class GetAccountStatusUseCase {
    private final MovementRepository movementRepository;

    public GetAccountStatusUseCase(MovementRepository movementRepository) {
        this.movementRepository = movementRepository;
    }

    public Page<AccountStatusResponse> execute(AccountStatusRequest request) {
        Pageable pageable = PageRequest.of(
                request.page(),
                request.size(),
                Sort.by("account.number").descending().and(Sort.by("date").descending())
        );
        Specification<Movement> specification = (root, query, criteriaBuilder) -> {
            Predicate predicate = criteriaBuilder.equal(root.get("account").get("number"), request.accountNumber());
            if (request.from() != null) {
                predicate = criteriaBuilder.and(
                        predicate,
                        criteriaBuilder.greaterThanOrEqualTo(root.get("date"), request.from())
                );
            }
            if (request.to() != null) {
                predicate = criteriaBuilder.and(
                        predicate,
                        criteriaBuilder.lessThanOrEqualTo(root.get("date"), request.to())
                );
            }
            return predicate;
        };
        return movementRepository.findAll(specification, pageable).map(AccountStatusResponse::from);
    }
}