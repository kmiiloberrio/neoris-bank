package local.technicaltest.neorisbank.movement.application.create;

public record MovementCreateRequest(
        Integer accountNumber,
        String movementType,
        Double amount
) {
}