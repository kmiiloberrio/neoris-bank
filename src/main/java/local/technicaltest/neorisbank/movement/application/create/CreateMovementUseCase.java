package local.technicaltest.neorisbank.movement.application.create;

import jakarta.transaction.Transactional;
import local.technicaltest.neorisbank.account.domain.Account;
import local.technicaltest.neorisbank.account.domain.AccountNotFoundException;
import local.technicaltest.neorisbank.account.infrastructure.persistence.AccountRepository;
import local.technicaltest.neorisbank.movement.domain.Movement;
import local.technicaltest.neorisbank.movement.infrastructure.persistence.MovementRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Transactional
public class CreateMovementUseCase {
    private final MovementRepository movementRepository;
    private final AccountRepository accountRepository;

    public CreateMovementUseCase(MovementRepository movementRepository, AccountRepository accountRepository) {
        this.movementRepository = movementRepository;
        this.accountRepository = accountRepository;
    }

    public void execute(MovementCreateRequest request) {
        Optional<Account> accountOptional = accountRepository.getReferenceByNumber(request.accountNumber());
        if (accountOptional.isEmpty()) {
            throw new AccountNotFoundException("Account not found");
        }

        Account account = accountOptional.get();
        Movement movement = Movement.create(
                account,
                request.movementType(),
                request.amount(),
                account.getCurrentBalance()
        );

        movementRepository.save(movement);
    }
}