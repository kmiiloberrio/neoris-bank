package local.technicaltest.neorisbank.movement.application.account_status;

import local.technicaltest.neorisbank.movement.domain.Movement;

import java.time.LocalDate;

public record AccountStatusResponse(
        LocalDate date,
        String client,
        Integer accountNumber,
        String accountType,
        Double initialBalance,
        Boolean state,
        Double movement,
        Double availableBalance

) {
    public static AccountStatusResponse from(Movement movement) {
        return new AccountStatusResponse(
                movement.getDate(),
                movement.getAccount().getClientName(),
                movement.getAccount().getAccountNumber(),
                movement.getAccount().getAccountType(),
                movement.getAccount().getInitialBalance(),
                movement.getAccount().getState(),
                movement.getAmount(),
                movement.getBalance()
        );
    }
}