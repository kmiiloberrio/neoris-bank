package local.technicaltest.neorisbank.movement.application.account_status;

import java.time.LocalDate;

public record AccountStatusRequest(
        LocalDate from,
        LocalDate to,
        Integer accountNumber,
        Integer page,
        Integer size
) {
}
