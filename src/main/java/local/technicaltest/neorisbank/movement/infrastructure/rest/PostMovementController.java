package local.technicaltest.neorisbank.movement.infrastructure.rest;

import local.technicaltest.neorisbank.movement.application.create.CreateMovementUseCase;
import local.technicaltest.neorisbank.movement.application.create.MovementCreateRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/movements")
public class PostMovementController {
    private final CreateMovementUseCase createMovementUseCase;

    public PostMovementController(CreateMovementUseCase createMovementUseCase) {
        this.createMovementUseCase = createMovementUseCase;
    }

    @PostMapping()
    public void createMovement(@RequestBody MovementCreateRequest request) {
        createMovementUseCase.execute(request);
    }
}
