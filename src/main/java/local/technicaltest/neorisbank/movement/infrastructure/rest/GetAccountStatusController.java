package local.technicaltest.neorisbank.movement.infrastructure.rest;

import local.technicaltest.neorisbank.movement.application.account_status.AccountStatusRequest;
import local.technicaltest.neorisbank.movement.application.account_status.AccountStatusResponse;
import local.technicaltest.neorisbank.movement.application.account_status.GetAccountStatusUseCase;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@RequestMapping("/reportes")
public class GetAccountStatusController {
    private final GetAccountStatusUseCase getAccountStatusUseCase;

    public GetAccountStatusController(GetAccountStatusUseCase getAccountStatusUseCase) {
        this.getAccountStatusUseCase = getAccountStatusUseCase;
    }

    @GetMapping()
    public Page<AccountStatusResponse> getAccountStatus(
            @RequestParam LocalDate from,
            @RequestParam LocalDate to,
            @RequestParam Integer accountNumber,
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer size
    ) {
        AccountStatusRequest request = new AccountStatusRequest(from, to, accountNumber, page, size);
        return getAccountStatusUseCase.execute(request);
    }
}