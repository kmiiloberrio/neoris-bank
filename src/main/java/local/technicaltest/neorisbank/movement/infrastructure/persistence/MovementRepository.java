package local.technicaltest.neorisbank.movement.infrastructure.persistence;

import local.technicaltest.neorisbank.movement.domain.Movement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface MovementRepository extends JpaRepository<Movement, Long>, JpaSpecificationExecutor<Movement> {
    Optional<Movement> findLastMovementByAccountNumber(Integer integer);
}