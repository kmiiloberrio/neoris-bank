package local.technicaltest.neorisbank.movement.infrastructure.rest;

import jakarta.servlet.http.HttpServletRequest;
import local.technicaltest.neorisbank.client.infrastructure.rest.ApiError;
import local.technicaltest.neorisbank.movement.domain.BalanceNotAvailableException;
import local.technicaltest.neorisbank.movement.domain.InvalidMovementTypeException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.text.SimpleDateFormat;


@RestControllerAdvice
public class MovementExceptionController {
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");

    @ExceptionHandler(BalanceNotAvailableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleException(BalanceNotAvailableException exception, HttpServletRequest request) {
        return new ApiError(
                simpleDateFormat.format(System.currentTimeMillis()),
                HttpStatus.BAD_REQUEST.value(),
                exception.getMessage(),
                request.getRequestURL().toString()
        );
    }

    @ExceptionHandler(InvalidMovementTypeException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleException(InvalidMovementTypeException exception, HttpServletRequest request) {
        return new ApiError(
                simpleDateFormat.format(System.currentTimeMillis()),
                HttpStatus.BAD_REQUEST.value(),
                exception.getMessage(),
                request.getRequestURL().toString()
        );
    }
}