package local.technicaltest.neorisbank.movement.domain;

public enum MovementType {
    WITHDRAW, DEPOSIT;

    public static MovementType fromString(String movementType) {
        for (MovementType type : MovementType.values()) {
            if (movementType.equalsIgnoreCase(type.name())) {
                return type;
            }
        }
        throw new InvalidMovementTypeException("Invalid movement type");
    }
}