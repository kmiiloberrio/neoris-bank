package local.technicaltest.neorisbank.movement.domain;

public class InvalidMovementTypeException extends RuntimeException{
    public InvalidMovementTypeException(String message) {
        super(message);
    }
}