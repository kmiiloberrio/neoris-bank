package local.technicaltest.neorisbank.movement.domain;

public class BalanceNotAvailableException extends RuntimeException {
    public BalanceNotAvailableException(String message) {
        super(message);
    }
}