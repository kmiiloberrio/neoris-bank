package local.technicaltest.neorisbank.movement.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import local.technicaltest.neorisbank.account.domain.Account;
import org.hibernate.proxy.HibernateProxy;

import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "movement")
public class Movement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "date", nullable = false)
    private LocalDate date;
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private MovementType type;
    @Column(name = "amount", nullable = false)
    private Double amount;
    @Column(name = "balance", nullable = false)
    private Double balance;
    @ManyToOne(optional = false)
    @JoinColumn(name = "number", nullable = false)
    private Account account;

    protected Movement() {
    }

    public Movement(LocalDate now, MovementType movementType, Double amount, Double balance, Account account) {
        this.date = now;
        this.type = movementType;
        this.amount = amount;
        this.balance = balance;
        this.account = account;
    }


    public static Movement create(Account account, String movementType, Double amount, Double balance) {
        if (account.getInitialBalance().equals(0.0)) {
            throw new BalanceNotAvailableException("Saldo no disponible");
        }

        if (movementType.equalsIgnoreCase("WITHDRAW")) {
            balance -= amount;
        }
        if (movementType.equalsIgnoreCase("DEPOSIT")) {
            balance += amount;
        }

        if (balance < 0.0) {
            throw new BalanceNotAvailableException("Saldo no disponible");
        }

        return new Movement(
                LocalDate.now(),
                MovementType.fromString(movementType),
                amount,
                balance,
                account
        );
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Class<?> oEffectiveClass =
                o instanceof HibernateProxy ? ((HibernateProxy) o).getHibernateLazyInitializer()
                        .getPersistentClass() : o.getClass();
        Class<?> thisEffectiveClass =
                this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer()
                        .getPersistentClass() : this.getClass();
        if (thisEffectiveClass != oEffectiveClass) return false;
        Movement movement = (Movement) o;
        return id != null && Objects.equals(id, movement.id);
    }

    @Override
    public final int hashCode() {
        return this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer()
                .getPersistentClass()
                .hashCode() : getClass().hashCode();
    }

    public LocalDate getDate() {
        return date;
    }

    public MovementType getType() {
        return type;
    }

    public Double getAmount() {
        return amount;
    }

    public Double getBalance() {
        return balance;
    }

    public Account getAccount() {
        return account;
    }

    public Long getId() {
        return this.id;
    }
}