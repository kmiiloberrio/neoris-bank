package local.technicaltest.neorisbank.account.infrastructure.rest;

import jakarta.servlet.http.HttpServletRequest;
import local.technicaltest.neorisbank.account.domain.AccountNotFoundException;
import local.technicaltest.neorisbank.account.domain.InvalidAccountTypeException;
import local.technicaltest.neorisbank.client.infrastructure.rest.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.text.SimpleDateFormat;

@RestControllerAdvice
public class AccountExceptionController {
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");

    @ExceptionHandler(InvalidAccountTypeException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleInvalidAccountTypeException(InvalidAccountTypeException ex, HttpServletRequest request) {
        return new ApiError(
                simpleDateFormat.format(System.currentTimeMillis()),
                HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(),
                request.getRequestURL().toString()
        );
    }

    @ExceptionHandler(AccountNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ApiError handleAccountNotFoundException(AccountNotFoundException ex, HttpServletRequest request) {
        return new ApiError(
                simpleDateFormat.format(System.currentTimeMillis()),
                HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(),
                request.getRequestURL().toString()
        );
    }
}