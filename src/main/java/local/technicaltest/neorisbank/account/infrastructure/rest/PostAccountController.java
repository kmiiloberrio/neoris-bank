package local.technicaltest.neorisbank.account.infrastructure.rest;

import local.technicaltest.neorisbank.account.application.create.AccountCreateRequest;
import local.technicaltest.neorisbank.account.application.create.CreateAccountUseCase;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/accounts")
public class PostAccountController {
    private final CreateAccountUseCase accountCreateUseCase;

    public PostAccountController(
            CreateAccountUseCase accountCreateUseCase1
    ) {
        this.accountCreateUseCase = accountCreateUseCase1;
    }

    @PostMapping()
    public void createAccount(@RequestBody AccountCreateRequest request) {
        accountCreateUseCase.execute(request);
    }
}
