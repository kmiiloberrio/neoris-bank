package local.technicaltest.neorisbank.account.infrastructure.persistence;

import local.technicaltest.neorisbank.account.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Integer>, JpaSpecificationExecutor<Account> {
    Optional<Account> getReferenceByNumber(Integer number);
}
