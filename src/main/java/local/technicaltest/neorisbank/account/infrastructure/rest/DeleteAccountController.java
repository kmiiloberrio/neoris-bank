package local.technicaltest.neorisbank.account.infrastructure.rest;

import local.technicaltest.neorisbank.account.application.delete.DeleteAccountUseCase;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/accounts")
public class DeleteAccountController {
    private final DeleteAccountUseCase deleteAccountUseCase;

    public DeleteAccountController(DeleteAccountUseCase deleteAccountUseCase) {
        this.deleteAccountUseCase = deleteAccountUseCase;
    }


    @DeleteMapping("/{accountNumber}")
    public void deleteAccount(@PathVariable Integer accountNumber) {
        deleteAccountUseCase.execute(accountNumber);
    }
}