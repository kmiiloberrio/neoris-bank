package local.technicaltest.neorisbank.account.infrastructure.rest;

import local.technicaltest.neorisbank.account.application.update.AccountUpdateRequest;
import local.technicaltest.neorisbank.account.application.update.UpdateAccountUseCase;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/accounts")
public class PutAccountController {
    private final UpdateAccountUseCase updateAccountUseCase;

    public PutAccountController(UpdateAccountUseCase updateAccountUseCase) {
        this.updateAccountUseCase = updateAccountUseCase;
    }

    @PutMapping()
    public void updateAccount(@RequestBody AccountUpdateRequest request) {
        updateAccountUseCase.execute(request);
    }
}
