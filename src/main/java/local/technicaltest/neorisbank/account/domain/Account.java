package local.technicaltest.neorisbank.account.domain;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import local.technicaltest.neorisbank.client.domain.Client;
import local.technicaltest.neorisbank.movement.domain.Movement;
import org.hibernate.proxy.HibernateProxy;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Entity
@Table(name = "account")
public class Account {
    @Id
    @Column(name = "number", nullable = false, unique = true, length = 11)
    private Integer number;
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private AccountType type;
    @Column(name = "initial_balance", nullable = false)
    private Double initialBalance;
    @Column(name = "state", nullable = false)
    private Boolean state;
    @ManyToOne(optional = false)
    @JoinColumn(name = "client_id", nullable = false)
    private Client client;
    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true)
    private final Set<Movement> movements = new LinkedHashSet<>();

    protected Account() {

    }

    protected Account(Integer number, AccountType type, Double initialBalance, Boolean state, Client client) {
        this.number = number;
        this.type = type;
        this.initialBalance = initialBalance;
        this.state = state;
        this.client = client;
    }

    public static Account create(
            Integer number,
            String type,
            Double initialBalance,
            Client client
    ) {
        return new Account(
                number,
                AccountType.fromString(type),
                initialBalance,
                true,
                client
        );
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Class<?> oEffectiveClass =
                o instanceof HibernateProxy ? ((HibernateProxy) o).getHibernateLazyInitializer()
                        .getPersistentClass() : o.getClass();
        Class<?> thisEffectiveClass =
                this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer()
                        .getPersistentClass() : this.getClass();
        if (thisEffectiveClass != oEffectiveClass) return false;
        Account account = (Account) o;
        return number != null && Objects.equals(number, account.number);
    }

    @Override
    public final int hashCode() {
        return this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer()
                .getPersistentClass()
                .hashCode() : getClass().hashCode();
    }

    public Integer getAccountNumber() {
        return this.number;
    }

    public String getAccountType() {
        return this.type.name();
    }

    public Boolean getState() {
        return this.state;
    }

    public Double getInitialBalance() {
        return this.initialBalance;
    }

    public void updateAccountType(String accountType) {
        if (accountType != null && !this.type.name().equals(accountType)) {
            this.type = AccountType.fromString(accountType);
        }
    }

    public void updateState(Boolean state) {
        if (state != null && !this.state.equals(state)) {
            this.state = state;
        }
    }

    public String getClientName() {
        return this.client.getName();
    }

    public Double getCurrentBalance() {
        Optional<Movement> lastMovement = this.movements
                .stream()
                .max(Comparator.comparing(Movement::getId));
        return lastMovement
                .map(Movement::getBalance)
                .orElse(this.initialBalance);
    }
}