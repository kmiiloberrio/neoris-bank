package local.technicaltest.neorisbank.account.domain;

public enum AccountType {
    AHORROS, CORRIENTE;

    public static AccountType fromString(String movementType) {
        for (AccountType type : AccountType.values()) {
            if (movementType.equalsIgnoreCase(type.name())) {
                return type;
            }
        }
        throw new InvalidAccountTypeException("Invalid account type");
    }
}