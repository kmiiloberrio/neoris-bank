package local.technicaltest.neorisbank.account.application.delete;

import jakarta.transaction.Transactional;
import local.technicaltest.neorisbank.account.domain.Account;
import local.technicaltest.neorisbank.account.domain.AccountNotFoundException;
import local.technicaltest.neorisbank.account.infrastructure.persistence.AccountRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Transactional
public class DeleteAccountUseCase {
    private final AccountRepository accountRepository;

    public DeleteAccountUseCase(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public void execute(Integer accountNumber) {
        Optional<Account> account = accountRepository.getReferenceByNumber(accountNumber);
        if (account.isEmpty()) {
            throw new AccountNotFoundException("Account not found");
        }

        accountRepository.delete(account.get());
    }
}