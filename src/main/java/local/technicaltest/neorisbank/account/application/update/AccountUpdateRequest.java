package local.technicaltest.neorisbank.account.application.update;

public record AccountUpdateRequest(
        Integer accountNumber,
        String accountType,
        Boolean state
) {
}