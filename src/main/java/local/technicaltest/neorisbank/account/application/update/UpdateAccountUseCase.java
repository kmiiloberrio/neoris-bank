package local.technicaltest.neorisbank.account.application.update;

import jakarta.transaction.Transactional;
import local.technicaltest.neorisbank.account.domain.Account;
import local.technicaltest.neorisbank.account.domain.AccountNotFoundException;
import local.technicaltest.neorisbank.account.infrastructure.persistence.AccountRepository;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class UpdateAccountUseCase {
    private final AccountRepository accountRepository;

    public UpdateAccountUseCase(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public AccountUpdateResponse execute(AccountUpdateRequest request) {
        Account account;
        try {
            account = accountRepository.getReferenceById(request.accountNumber());
        } catch (Exception e) {
            throw new AccountNotFoundException("Account not found");
        }

        account.updateAccountType(request.accountType());
        account.updateState(request.state());

        accountRepository.save(account);

        return AccountUpdateResponse.from(account);
    }
}