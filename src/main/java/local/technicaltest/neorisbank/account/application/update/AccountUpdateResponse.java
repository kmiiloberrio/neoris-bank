package local.technicaltest.neorisbank.account.application.update;

import local.technicaltest.neorisbank.account.domain.Account;
import local.technicaltest.neorisbank.client.domain.Client;

import java.util.UUID;

public record AccountUpdateResponse(
        Integer accountNumber,
        String accountType,
        Boolean state
) {
    public static AccountUpdateResponse from(Account account) {
        return new AccountUpdateResponse(
                account.getAccountNumber(),
                account.getAccountType(),
                account.getState()
        );
    }
}