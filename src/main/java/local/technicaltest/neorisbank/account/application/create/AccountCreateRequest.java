package local.technicaltest.neorisbank.account.application.create;

import java.util.UUID;

public record AccountCreateRequest(
        Integer accountNumber,
        String accountType,
        Double initialBalance,
        UUID clientId
) {
}