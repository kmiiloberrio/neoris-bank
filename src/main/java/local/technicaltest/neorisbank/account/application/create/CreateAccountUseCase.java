package local.technicaltest.neorisbank.account.application.create;

import jakarta.transaction.Transactional;
import local.technicaltest.neorisbank.account.domain.Account;
import local.technicaltest.neorisbank.account.infrastructure.persistence.AccountRepository;
import local.technicaltest.neorisbank.client.domain.Client;
import local.technicaltest.neorisbank.client.domain.ClientNotFoundException;
import local.technicaltest.neorisbank.client.infrastructure.persistence.ClientRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Transactional
public class CreateAccountUseCase {
    private final AccountRepository accountRepository;
    private final ClientRepository clientRepository;

    public CreateAccountUseCase(AccountRepository repository, ClientRepository clientRepository) {
        this.accountRepository = repository;
        this.clientRepository = clientRepository;
    }

    public void execute(AccountCreateRequest request) {
        Optional<Client> optionalClient = clientRepository.getReferenceByClientId(request.clientId());
        if (optionalClient.isEmpty()) {
            throw new ClientNotFoundException("Client not found");
        }

        Account account = Account.create(
                request.accountNumber(),
                request.accountType(),
                request.initialBalance(),
                optionalClient.get()
        );

        accountRepository.save(account);
    }
}