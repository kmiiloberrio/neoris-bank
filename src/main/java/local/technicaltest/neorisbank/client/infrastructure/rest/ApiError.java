package local.technicaltest.neorisbank.client.infrastructure.rest;

public class ApiError {
    private final String timeStamp;
    private final Integer errorCode;
    private final String errorMessage;
    private final String servicePath;

    public ApiError(String timeStamp, Integer errorCode, String errorMessage, String servicePath) {
        this.timeStamp = timeStamp;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.servicePath = servicePath;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getServicePath() {
        return servicePath;
    }
}