package local.technicaltest.neorisbank.client.infrastructure.rest;

import local.technicaltest.neorisbank.client.application.update.ClientUpdateRequest;
import local.technicaltest.neorisbank.client.application.update.ClientUpdateResponse;
import local.technicaltest.neorisbank.client.application.update.UpdateClientUseCase;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/clients")
public class PutClientController {
    private final UpdateClientUseCase updateClientUseCase;

    public PutClientController(UpdateClientUseCase updateClientUseCase) {
        this.updateClientUseCase = updateClientUseCase;
    }

    @PutMapping()
    public ClientUpdateResponse updateClient(@RequestBody ClientUpdateRequest request) {
        return updateClientUseCase.execute(request);
    }
}