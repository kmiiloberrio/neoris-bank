package local.technicaltest.neorisbank.client.infrastructure.rest;

import local.technicaltest.neorisbank.client.application.delete.DeleteClientUseCase;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/clients")
public class DeleteClientController {
    private final DeleteClientUseCase deleteClientUseCase;

    public DeleteClientController(DeleteClientUseCase deleteClientUseCase) {
        this.deleteClientUseCase = deleteClientUseCase;
    }

    @DeleteMapping("/{clientId}")
    public void createClient(@PathVariable UUID clientId) {
        deleteClientUseCase.execute(clientId);
    }
}