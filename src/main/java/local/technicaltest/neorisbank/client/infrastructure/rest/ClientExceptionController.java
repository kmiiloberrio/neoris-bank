package local.technicaltest.neorisbank.client.infrastructure.rest;

import jakarta.servlet.http.HttpServletRequest;
import local.technicaltest.neorisbank.client.domain.ClientNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.text.SimpleDateFormat;

@RestControllerAdvice
public class ClientExceptionController {
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");

    @ExceptionHandler(ClientNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ApiError handleClientNotFoundException(
            ClientNotFoundException clientNotFoundException,
            HttpServletRequest request
    ) {
        return new ApiError(
                simpleDateFormat.format(System.currentTimeMillis()),
                HttpStatus.NOT_FOUND.value(),
                clientNotFoundException.getMessage(),
                request.getRequestURL().toString()
        );
    }
}