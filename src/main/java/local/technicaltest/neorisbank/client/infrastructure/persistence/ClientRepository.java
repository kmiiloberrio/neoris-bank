package local.technicaltest.neorisbank.client.infrastructure.persistence;

import local.technicaltest.neorisbank.client.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ClientRepository extends JpaRepository<Client, Integer> {
    void deleteByClientId(UUID clientId);

    Optional<Client> getReferenceByClientId(UUID uuid);
}
