package local.technicaltest.neorisbank.client.infrastructure.rest;

import local.technicaltest.neorisbank.client.application.create.ClientRequest;
import local.technicaltest.neorisbank.client.application.create.CreateClientUseCase;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/clients")
public class PostClientController {
    private final CreateClientUseCase createClientUseCase;

    public PostClientController(CreateClientUseCase createClientUseCase) {
        this.createClientUseCase = createClientUseCase;
    }

    @PostMapping()
    public void createClient(@RequestBody ClientRequest request) {
        createClientUseCase.execute(request);
    }
}