package local.technicaltest.neorisbank.client.application.update;

import local.technicaltest.neorisbank.client.domain.Client;

import java.util.UUID;

public record ClientUpdateResponse(
        UUID clientId,
        String name,
        Character gender,
        Integer age,
        String identification,
        String address,
        Integer phone,
        String password,
        Boolean state
) {
    public static ClientUpdateResponse from(Client client) {
        return new ClientUpdateResponse(
                client.getClientId(),
                client.getName(),
                client.getGender(),
                client.getAge(),
                client.getIdentification(),
                client.getAddress(),
                client.getPhone(),
                client.getPassword(),
                client.getState()
        );
    }
}