package local.technicaltest.neorisbank.client.application.update;

import jakarta.transaction.Transactional;
import local.technicaltest.neorisbank.client.domain.Client;
import local.technicaltest.neorisbank.client.domain.ClientNotFoundException;
import local.technicaltest.neorisbank.client.infrastructure.persistence.ClientRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Transactional
public class UpdateClientUseCase {
    private final ClientRepository clientRepository;

    public UpdateClientUseCase(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public ClientUpdateResponse execute(ClientUpdateRequest request) {
        Optional<Client> optionalClient = clientRepository.getReferenceByClientId(request.clientId());
        if (optionalClient.isEmpty()) {
            throw new ClientNotFoundException("Client not found");
        }

        Client client = optionalClient.get();
        client.updateClientId(request.clientId());
        client.updateName(request.name());
        client.updateGender(request.gender());
        client.updateAge(request.age());
        client.updateIdentification(request.identification());
        client.updateAddress(request.address());
        client.updatePhone(request.phone());
        client.updatePassword(request.password());
        client.updateState(request.state());

        clientRepository.save(client);

        return ClientUpdateResponse.from(client);
    }
}