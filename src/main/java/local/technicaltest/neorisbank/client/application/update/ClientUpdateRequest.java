package local.technicaltest.neorisbank.client.application.update;

import java.util.UUID;

public record ClientUpdateRequest(
        UUID clientId,
        String name,
        Character gender,
        Integer age,
        String identification,
        String address,
        Integer phone,
        String password,
        Boolean state
) {
}