package local.technicaltest.neorisbank.client.application.create;

import java.util.UUID;

public record ClientRequest(
        UUID clientId,
        String name,
        Character gender,
        Integer age,
        String identification,
        String address,
        Integer phone,
        String password
) {
}