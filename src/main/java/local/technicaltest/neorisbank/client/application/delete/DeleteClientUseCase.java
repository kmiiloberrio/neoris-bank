package local.technicaltest.neorisbank.client.application.delete;

import jakarta.transaction.Transactional;
import local.technicaltest.neorisbank.client.infrastructure.persistence.ClientRepository;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Transactional
public class DeleteClientUseCase {
    private final ClientRepository clientRepository;

    public DeleteClientUseCase(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public void execute(UUID clientId) {
        clientRepository.deleteByClientId(clientId);
    }
}
