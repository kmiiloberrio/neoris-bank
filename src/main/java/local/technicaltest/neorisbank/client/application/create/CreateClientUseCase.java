package local.technicaltest.neorisbank.client.application.create;

import jakarta.transaction.Transactional;
import local.technicaltest.neorisbank.client.domain.Client;
import local.technicaltest.neorisbank.client.infrastructure.persistence.ClientRepository;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class CreateClientUseCase {
    private final ClientRepository clientRepository;

    public CreateClientUseCase(ClientRepository repository) {
        this.clientRepository = repository;
    }

    public void execute(ClientRequest request) {
        Client client = Client.create(
                request.clientId(),
                request.name(),
                request.gender(),
                request.age(),
                request.identification(),
                request.address(),
                request.phone(),
                request.password()
        );

        clientRepository.save(client);
    }
}
