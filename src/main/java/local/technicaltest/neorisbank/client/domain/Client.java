package local.technicaltest.neorisbank.client.domain;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import local.technicaltest.neorisbank.account.domain.Account;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@Entity(name = "client")
@Table(name = "client")
public class Client extends Person {
    @Column(name = "client_id", unique = true, nullable = false)
    private UUID clientId;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "state", nullable = false)
    private Boolean state;
    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL, orphanRemoval = true)
    private final Set<Account> accounts = new LinkedHashSet<>();

    protected Client() {
    }

    protected Client(
            UUID clientId,
            String name,
            Character gender,
            Integer age,
            String identification,
            String address,
            Integer phone,
            String password,
            Boolean state
    ) {
        this.clientId = clientId;
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.identification = identification;
        this.address = address;
        this.phone = phone;
        this.password = password;
        this.state = state;
    }

    public static Client create(
            UUID clientId,
            String name,
            Character gender,
            Integer age,
            String identification,
            String address,
            Integer phone,
            String password
    ) {
        return new Client(
                clientId,
                name,
                gender,
                age,
                identification,
                address,
                phone,
                password,
                true
        );
    }

    public void updateClientId(UUID clientId) {
        if (clientId != null && !this.clientId.equals(clientId)) {
            this.clientId = clientId;
        }
    }

    public void updatePassword(String password) {
        if (password != null && !this.password.equals(password)) {
            this.password = password;
        }
    }

    public void updateState(Boolean state) {
        if (state != null && !this.state.equals(state)) {
            this.state = state;
        }
    }

    public UUID getClientId() {
        return clientId;
    }

    public String getPassword() {
        return password;
    }

    public Boolean getState() {
        return state;
    }

    public Set<Account> getAccounts() {
        return accounts;
    }
}