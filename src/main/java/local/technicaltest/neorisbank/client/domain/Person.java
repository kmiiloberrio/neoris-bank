package local.technicaltest.neorisbank.client.domain;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    protected Long id;
    @Column(name = "name", nullable = false, length = 50)
    protected String name;
    @Column(name = "gender", nullable = false, length = 1)
    protected Character gender;
    @Column(name = "age", nullable = false)
    protected Integer age;
    @Column(name = "identification", nullable = false)
    protected String identification;
    @Column(name = "address", nullable = false)
    protected String address;
    @Column(name = "phone", nullable = false)
    protected Integer phone;

    public void updateName(String name) {
        if (name != null && !this.name.equals(name)) {
            this.name = name;
        }
    }

    public void updateGender(Character gender) {
        if (gender != null && !this.gender.equals(gender)) {
            this.gender = gender;
        }
    }

    public void updateAge(Integer age) {
        if (age != null && !this.age.equals(age)) {
            this.age = age;
        }
    }

    public void updateIdentification(String identification) {
        if (identification != null && !this.identification.equals(identification)) {
            this.identification = identification;
        }
    }

    public void updateAddress(String address) {
        if (address != null && !this.address.equals(address)) {
            this.address = address;
        }
    }

    public void updatePhone(Integer phone) {
        if (phone != null && !this.phone.equals(phone)) {
            this.phone = phone;
        }
    }

    public String getName() {
        return name;
    }

    public Character getGender() {
        return gender;
    }

    public Integer getAge() {
        return age;
    }

    public String getIdentification() {
        return identification;
    }

    public String getAddress() {
        return address;
    }

    public Integer getPhone() {
        return phone;
    }
}