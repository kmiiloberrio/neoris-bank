package local.technicaltest.neorisbank.movement.infrastructure.rest;

import local.technicaltest.neorisbank.movement.application.account_status.AccountStatusResponse;
import local.technicaltest.neorisbank.movement.application.account_status.GetAccountStatusUseCase;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(GetAccountStatusController.class)
class GetAccountStatusControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GetAccountStatusUseCase getAccountStatusUseCase;

    @Test
    public void getEmptyStatusReportForDateRange() throws Exception {
        Mockito.when(getAccountStatusUseCase.execute(any())).thenReturn(new PageImpl<>(new ArrayList<>()));

        mockMvc.perform(MockMvcRequestBuilders.get("/reportes")
                        .param("from", LocalDate.now().toString())
                        .param("to", LocalDate.now().toString())
                        .param("accountNumber", "1")
                        .param("page", "0")
                        .param("size", "10"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(0)));
    }

    @Test
    public void getStatusReportForDateRange() throws Exception {
        AccountStatusResponse accountStatusResponse = new AccountStatusResponse(
                LocalDate.now(), "neoris", 1, "test", 100.0, true, 100.0, 100.0
        );
        List<AccountStatusResponse> responseList = Collections.singletonList(accountStatusResponse);
        Mockito.when(getAccountStatusUseCase.execute(any())).thenReturn(new PageImpl<>(responseList));

        mockMvc.perform(MockMvcRequestBuilders.get("/reportes")
                        .param("from", LocalDate.now().toString())
                        .param("to", LocalDate.now().toString())
                        .param("accountNumber", "1")
                        .param("page", "0")
                        .param("size", "10"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content[0].date").value("2023-08-07"))
                .andExpect(jsonPath("$.content[0].client").value("neoris"))
                .andExpect(jsonPath("$.content[0].accountNumber").value(1))
                .andExpect(jsonPath("$.content[0].accountType").value("test"))
                .andExpect(jsonPath("$.content[0].initialBalance").value(100.0))
                .andExpect(jsonPath("$.content[0].state").value(true))
                .andExpect(jsonPath("$.content[0].movement").value(100.0))
                .andExpect(jsonPath("$.content[0].availableBalance").value(100.0))
                .andExpect(jsonPath("$.pageable").value("INSTANCE"))
                .andExpect(jsonPath("$.totalPages").value(1))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.last").value(true))
                .andExpect(jsonPath("$.size").value(1))
                .andExpect(jsonPath("$.number").value(0))
                .andExpect(jsonPath("$.sort.empty").value(true))
                .andExpect(jsonPath("$.numberOfElements").value(1))
                .andExpect(jsonPath("$.first").value(true))
                .andExpect(jsonPath("$.empty").value(false));

    }
}