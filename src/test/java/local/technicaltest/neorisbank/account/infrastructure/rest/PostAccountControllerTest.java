package local.technicaltest.neorisbank.account.infrastructure.rest;

import local.technicaltest.neorisbank.account.application.create.CreateAccountUseCase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PostAccountController.class)
class PostAccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CreateAccountUseCase accountCreateUseCase;

    @Test
    void createAccount() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/accounts")
                        .contentType("application/json")
                        .content("{\n" +
                                "  \"accountNumber\": 478758,\n" +
                                "  \"accountType\": \"ahorros\",\n" +
                                "  \"initialBalance\": 2000,\n" +
                                "  \"clientId\": \"4fc91ccf-801b-4526-b33a-49842e2f7d99\"\n" +
                                "}"))
                .andExpect(status().isOk());
    }
}